/*-- [知识点1]--可执行程序必须是在main package下
  包名与导入路径的最后一个元素一致，并不需要唯一，相反地引入路径必须是唯一的。
*/
package main

import (
	"fmt"
	"time"
	"math"
)

func priateFunc(x int, y int) int{
	return x * y
}

func fun2(x, y int) int{
	return x + y
}

func swap(x, y string) (string, string) {
	return y, x
}

func split(sum int) (x, y int) {
	x = sum * 4 / 9
	y = sum - x
	return
}

func main(){

	fmt.Println(math.Pi) // [知识点2] 大写字母开头的名称代表它已经从包中导出了，可以被外部访问


	var a = "hello"
	var b = "world"

	var c bool = true
	var d byte = 1 << 8 - 1 
	var e int = 2
	var f int8 = -128
	var g int16 = 1 << 15 - 1 

	fmt.Println(a + b) // 连接字符串可以用加号

	fmt.Println("当前时间：", time.Now().Format(time.RFC3339))
	fmt.Println("当前时间：", time.Now().Format(time.RFC3339))

	fmt.Println(c, d, e, f, g)
}